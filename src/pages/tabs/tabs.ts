import  { Component, OnInit } from '@angular/core';
import { AppareilPage } from '../appareils/appareil';
import { SettingsPage } from '../settings/settings';

@Component({
    selector: 'page-tabs',
    templateUrl: 'tabs.html'
})
export class TabsPage implements OnInit {

    appareilsPage = AppareilPage;
    settingsPage = SettingsPage;
    constructor() { }

    ngOnInit() { 

    }

}